package coursecontroller

import (
	"go-udemy/models"

	"github.com/gofiber/fiber/v2"
)

func Index(c *fiber.Ctx) error {
	rows, err := models.DB.Query("SELECT * FROM course")
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}
	defer rows.Close()
	result := models.Courses{}

	for rows.Next() {
		courses := models.Course{}
		if err := rows.Scan(&courses.Id, &courses.Title, &courses.Subtitle, &courses.Creator, &courses.DateCreated, &courses.Language, &courses.Price, &courses.Learned); err != nil {
			return err
		}

		result.Courses = append(result.Courses, courses)
	}
	return c.JSON(result)
}

func Login(c *fiber.Ctx) error {
	// coba := new(models.User)
	rows, err := models.DB.Query("SELECT * FROM users WHERE username=$1 AND pwd=$2", "latifasalsb", "secret")
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}
	defer rows.Close()
	result := models.Users{}

	for rows.Next() {
		users := models.User{}
		if err := rows.Scan(&users.Id, &users.Username, &users.Password); err != nil {
			return err
		}

		result.Users = append(result.Users)
	}
	return c.JSON(rows)
}

func Update(c *fiber.Ctx) error {
	return nil
}

func Delete(c *fiber.Ctx) error {
	return nil
}
