package main

import (
	"log"

	"go-udemy/controllers/coursecontroller"
	"go-udemy/models"

	"github.com/gofiber/fiber/v2"

	_ "github.com/lib/pq"
)

func main() {

	if err := models.Connect(); err != nil {
		log.Fatal(err)
	}

	app := fiber.New()

	app.Get("/api/get", coursecontroller.Index)
	app.Post("/api/login", coursecontroller.Login)

	app.Listen(":8000")
}
