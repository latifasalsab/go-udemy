package models

type Course struct {
	Id          int64  `gorm:"primaryKey" json:"id"`
	Title       string `gorm:"type:varchar(255)" json:"title"`
	Subtitle    string `gorm:"type:varchar(255)" json:"subtitle"`
	Creator     string `gorm:"type:varchar(255)" json:"creator"`
	DateCreated string `gorm:"type:datetime" json:"datecreated"`
	Language    string `gorm:"type:varchar(255)" json:"languange"`
	Price       int64  `gorm:"type:int4" json:"price"`
	Learned     int64  `gorm:"type:int4" json:"learned"`
}

type Courses struct {
	Courses []Course `json:"courses"`
}
